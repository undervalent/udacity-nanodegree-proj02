var bio = {
    name          : "Jon Ochoa",
    role          : "UX/UI Designer",
    contacts      : {
        mobile  : "650.590.5507",
        email   : "undervalent@gmail.com",
        github  : "undervalent",
        twitter : "@0078a",
        location: "Campbell CA"
    },
    welcomeMessage: "Hi my name is Jon Ochoa.",
    skills        : ["Javascript", "AngularJS", "CSS", "SASS/Compass", "HTML", "UX/UI Design"],
    biopic        : "https://media.licdn.com/mpr/mpr/shrinknp_400_400/p/2/005/06c/15b/1a11951.jpg",
    display       : function () {
        var header = $('#header');

        //bio
        var name = HTMLheaderName.replace('%data%', bio.name);
        var role = HTMLheaderRole.replace('%data%', bio.role);
        var biopic = HTMLbioPic.replace('%data%', bio.biopic);
        var bioMessage = HTMLwelcomeMsg.replace('%data%', bio.welcomeMessage);
        header.append(biopic).append(bioMessage).prepend(role).prepend(name);

        //Contacts
        var mobile = HTMLmobile.replace('%data%', bio.contacts.mobile);
        var email = HTMLemail.replace('%data%', bio.contacts.email);
        var twitter = HTMLtwitter.replace('%data%', bio.contacts.twitter);
        var git = HTMLgithub.replace('%data%', bio.contacts.github);
        var location = HTMLlocation.replace('%data%', bio.contacts.location);
        $('#topContacts').append(mobile, email, twitter, git, location);

        if (bio.skills.length > 0) {
            header.append(HTMLskillsStart);
            var skills = $('#skills');
            for (var i = 0, len = bio.skills.length; i < len; i++) {
                var skillHtml = HTMLskills.replace('%data%', bio.skills[i]);
                skills.append(skillHtml);
            }
        }
    }
};

var education = {
    schools               : [
        {
            name    : "Academy of Art University",
            location: "San Francisco, CA",
            degree  : "None",
            majors  : ["Graphic Design"],
            dates   : "some dates - some other dates",
            url     : "http://www.academyart.edu/"

        },
        {
            name    : "College of San Mateo",
            location: "San Mateo, CA",
            degree  : "AS",
            majors  : ["Electronics"],
            dates   : "A long time a go",
            url     : "http://collegeofsanmateo.edu/"

        }
    ],
    onlineCourses         : [
        {
            title : "Programming for Everybody (Python)",
            school: "Coursera",
            date  : "8/24/2014",
            url   : "https://www.coursera.org/course/pythonlearn"
        },
        {
            title : "Udemy: From Fundamentals to Functional JS",
            school: "Udemy",
            date  : "4/2015",
            url   : "hhttps://www.udemy.com/certificate/UC-GF6NP71Y"
        }
    ],
    displayEducation      : function () {
        var educationDiv = $('#education');
        var schools = education.schools;

        for (var i = 0; i < schools.length; i++) {
            educationDiv.append(HTMLschoolStart);
            var entryContainer = $('.education-entry:last');
            var name = HTMLschoolName.replace('%data%', schools[i].name);
            var location = HTMLschoolLocation.replace('%data%', schools[i].location);
            var degree = HTMLschoolDegree.replace('%data%', schools[i].degree);
            var dates = HTMLschoolDates.replace('%data%', schools[i].dates);
            entryContainer.append(name + degree, location, dates);

            for (var j = 0; j < schools[i].majors.length; j++) {
                var major = HTMLschoolMajor.replace('%data%', schools[i].majors[j]);
                entryContainer.append(major);
            }
        }
    },
    displayOnlineEducation: function () {
        var educationDiv = $('#education');
        var onlineCourses = education.onlineCourses;
        educationDiv.append(HTMLonlineClasses);

        for (var i = 0; i < onlineCourses.length; i++) {
            educationDiv.append(HTMLschoolStart);
            var entryContainer = $('.education-entry:last');

            var title = HTMLonlineTitle.replace('%data%', onlineCourses[i].title);
            var school = HTMLonlineSchool.replace('%data%', onlineCourses[i].school);
            var date = HTMLonlineDates.replace('%data%', onlineCourses[i].date);
            var url = HTMLonlineURL.replace('%data%', onlineCourses[i].url);

            entryContainer.append(title + school, date, url);
            educationDiv.append(entryContainer);
        }
    },
    display : function () {
        this.displayEducation();
        this.displayOnlineEducation();
    }
};

var work = {
    jobs   : [
        {
            employer   : "Independant Contractor",
            title      : "Design/Development",
            location   : "Campbell, CA",
            dates      : "2001 - current",
            description: "Developed multiple Drupal websites including a commerce solution for public ticket sales of productions.Developed book demos in Flash http://astronomy.brookscole.com/seeds8e/demo.html for Thompson Brooks Cole, a publishing division of Cengage. Created amusing five-minute promotional video for a non-profit organization incorporating music and interviews with volunteers and children. Received with great enthusiasm. Designed and produced 13, 12-minute InDesign training videos for Chapter Three, a Drupal company in San Francisco, CA. Trained employees in basic and advanced levels of InDesign software."
        },
        {
            employer   : "Topix",
            title      : "UX/UI Developer",
            location   : "Palo Alto, CA",
            dates      : "3/2013 - current",
            description: "I build forward facing stuff and products for our editorial team."
        },
        {
            employer   : "Fig",
            title      : "Developer",
            location   : "Palo Alto, CA",
            dates      : "8/13/2012 - 3/2013",
            description: "Helped launch the Fig app; which is a mobile-first platform for holistic wellness coaching, tracking, and accountability, on IOS and Android platforms, using Sencha HTML5 platform with SASS and Compass. Took over design and helped develop Fig 2.0 pilot, enabling best-selling authors and experts to present daily progressive wellness experiences. Developed the responsive fig.com website in Drupal."
        }
    ],
    display: function () {
        var experience = $('#workExperience');
        experience.append(HTMLworkStart);
        var i = 0;
        while (i < work.jobs.length) {
            var employer = HTMLworkEmployer.replace('%data%', work.jobs[i].employer);
            var title = HTMLworkTitle.replace('%data%', work.jobs[i].title);
            var location = HTMLworkLocation.replace('%data%', work.jobs[i].location);
            var dates = HTMLworkDates.replace('%data%', work.jobs[i].dates);
            var description = HTMLworkDescription.replace('%data%', work.jobs[i].description);
            var line1 = employer + title;
            $('.work-entry').append(line1, location, dates, description);
            i++;
        }

    }
};

var projects = {

    projects: [
        {
            title      : "offbeat.topix.com",
            dates      : "1/2014 - present",
            description: "Front End Development for responsive Offbeat site.",
            images     : ["images/197x148.gif", "images/197x148.gif", "images/197x148.gif"]
        },
        {
            title      : "C3SV",
            dates      : "11/2013 - 01/2014",
            description: "Volunteered development time to help build a responsive Drupal site.",
            images     : ["images/197x148.gif", "images/197x148.gif", "images/197x148.gif"]
        }
    ],
    display : function () {
        var projectsDiv = $('#projects');
        var proj = projects.projects;
        projectsDiv.append(HTMLprojectStart);

        var i = 0;
        while (i < proj.length) {
            var title = HTMLprojectTitle.replace('%data%', proj[i].title);
            var date = HTMLprojectDates.replace('%data%', proj[i].dates);
            var description = HTMLprojectDescription.replace('%data%', proj[i].description);
            var entry = $('.project-entry').last();
            entry.append(title, date, description);

            for (var j = 0; j < proj[i].images.length; j++) {
                console.log(proj[i].images[j]);
                var image = HTMLprojectImage.replace('%data%', proj[i].images[j]);
                entry.append(image);
            }

            i += 1;
        }

    }
};

bio.display();
work.display();
projects.display();
education.display();
$('#mapDiv').append(googleMap);